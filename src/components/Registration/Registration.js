import React from 'react'
import './Registration.css'
import { Input } from '../UI/Input/Input.js'
import { FormSubmit } from '../FormSubmit/FormSubmit.js'
import { Message } from '../UI/Message/Message'


export class Registration extends React.Component {
  state = {
    userLogin: '',
    userPassword: '',
    userPasswordCopy: '',
    isValid: false,
    isError: false,
    errorText: ''
  }

  onSubmitHandler = event => {
    event.preventDefault()
  }

  updateUserLogin = value => {
    // if (value.lenght >=5) {
      this.setState({
        userLogin: value
      })
    // }
    // return
    
  }

  updateUserPassword = value => {
    this.setState({
      userPassword: value
    })
  }

  updateUserPasswordCopy = value => {
    this.setState({
      userPasswordCopy: value
    })
  }

  resetIsError = () => {
    this.setState({
      isError: false,
      errorText: ''
    })
  }

  
  isValidData = async () => {
    const login = this.state.userLogin
    const password = this.state.userPassword
    const passwordCopy = this.state.userPasswordCopy

    if (!(login && password && passwordCopy)) {
      this.setState({
        isError: true,
        errorText: 'Заполните все поля'
      })
      return
    }

    if (password !== passwordCopy) {
      this.setState({
        isError: true,
        errorText: 'Пароли не совпадают'
      })
      return
    }

    const users = await this.props.Api.getUsers()
    for (const user in users) {
      if (login === users[user].login) {
        this.setState({
          isError: true,
          errorText: 'Позьзователь с таким логином уже существует'
        })
        return
      }
    }

    this.setState({
      isValid: true
    })
  }


  async componentDidUpdate() {
    if (this.state.isValid) {
      const login = this.state.userLogin
      const password = this.state.userPassword
      await this.props.Api.addUser(login, password)
      this.timer = setTimeout(() => {
        this.props.history.push('/auth')
      }, 2000)
    }
  }


  componentWillUnmount() {
    clearTimeout(this.timer)
  }


  render() {
    return (
      <form onChange={this.resetIsError} onSubmit={this.onSubmitHandler} className='form'>
        <Input
          onBlur={this.updateUserLogin}
          id='login'
          name='login'
          htmlFor='login'
          label='Логин'
          type='text'
          placeholder='Придумайте логин'
        />

        <Input
          onBlur={this.updateUserPassword}
          id='password'
          name='password'
          htmlFor='password'
          label='Пароль'
          type='password'
          placeholder='Придумайте пароль'
        />

        <Input
          onBlur={this.updateUserPasswordCopy}
          id='passwordAgain'
          name='passwordAgain'
          htmlFor='passwordAgain'
          label='Повторите пароль'
          type='password'
          placeholder='Введите пароль еще раз'
        />

        <FormSubmit
          onClick={this.isValidData}
          text='Зарегистрироваться'
          orText='войти'
          to='/auth'
        />

        {this.state.isError ?
          <Message
            text={this.state.errorText}
            className = 'error'
          />
          : null}

        {this.state.isValid ?
          <Message
            text='Пользователь создан'
            className = 'success'
          />
          : null}

      </form>
    )
  }
}