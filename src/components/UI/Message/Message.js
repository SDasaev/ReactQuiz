import React from 'react'
import './Message.css'


export function Message(props) {
  return (
  <p className={props.className}>{props.text}</p>
  )
}