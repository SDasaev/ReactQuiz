import React from 'react'
import './Input.css'

export function Input(props) {
  return (
    <div className='input_wrapper'>

      <label className='label' htmlFor={props.htmlFor}>{props.label}</label>
      <input
        onBlur={event => {
          props.onBlur(event.target.value)
        }}
        className='input'
        type={props.type}
        name={props.name}
        id={props.id}
        placeholder={props.placeholder}>
      </input>

    </div>
  )
}