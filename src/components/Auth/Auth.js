import React from 'react'
import './Auth.css'
import { Input } from '../UI/Input/Input.js'
import { FormSubmit } from '../FormSubmit/FormSubmit.js'
import { Loader } from '../UI/Loader/Loader.js'
import { Message } from '../UI/Message/Message.js'


export class Auth extends React.Component {

  onSubmitHandler = event => {
    event.preventDefault()
  }

  goToCabinetPage = (history) => {
    history.push('/lk')
  }

  componentWillUnmount() {
    this.props.resetIsError()
  }

  componentDidUpdate() {
    this.props.isLogin ? this.goToCabinetPage(this.props.history) : null
  }


  render() {
    return (
      <form onSubmit={this.onSubmitHandler} className='form'>

        <Input
          onBlur={this.props.updateUserLogin}
          id='login'
          name='login'
          htmlFor='login'
          label='Логин'
          type='text'
          placeholder='Введите ваш логин'
        />

        <Input
          onBlur={this.props.updateUserPassword}
          id='password'
          name='password'
          htmlFor='password'
          label='Пароль'
          type='password'
          placeholder='Введите Ваш пароль'
        />

        <FormSubmit
          onClick={this.props.updateIsLogin}
          text='Войти'
          orText='зарегистрироваться'
          to='/registration'
        />

        {this.props.loading ? <Loader /> : null}
        {this.props.isError ? 
          <Message
            text = {this.props.errorText}
            className = 'error' 
          /> 
          : null}
      </form>
    )
  }
}