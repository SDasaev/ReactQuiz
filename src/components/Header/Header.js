import React from 'react'
import './Header.css'
import { NavLink } from 'react-router-dom'


export class Header extends React.Component {


  render() {
    return (
      <header className='header'>
        <div className='header_content'>
          <h1 className='title'>React-Quiz</h1>
          <div className='right_column'>
            <NavLink
              to={this.props.isLogin ? '/lk' : '/registration'}
              className='link'
            >{this.props.isLogin ? 'Личный кабинет' : 'Зарегистрироваться'}
            </NavLink>

            <NavLink
              onClick={() => {
                if (this.props.isLogin) {
                  this.props.logout()
                }
              }}

              to={this.props.isLogin ? '/' : '/auth'}
              className='link'
            >{this.props.isLogin ? 'Выйти' : 'Войти'}
            </NavLink>
          </div>
        </div>
      </header>
    )
  }
}