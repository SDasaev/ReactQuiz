import React from 'react' 
import './FormSubmit.css'
import {Button} from '../UI/Button/Button.js'
import {NavLink} from 'react-router-dom'




export function FormSubmit(props) {
  return (
    <div className='submit_wrapper'>
        <Button 
          onClick={props.onClick}
        >{props.text}</Button>
        <span> или </span>
        <NavLink to = {props.to} className='link'>{props.orText}</NavLink>
      </div>
  )
}