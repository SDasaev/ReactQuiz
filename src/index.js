import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
import { BrowserRouter } from 'react-router-dom'
import { Api } from './api.js';
import './index.css'


const application = (
  <BrowserRouter>
    <App Api={Api}  />
  </BrowserRouter>
)

ReactDOM.render(application, document.getElementById('root'));
