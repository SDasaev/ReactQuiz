import React from 'react';
import { Route } from 'react-router-dom'
import { Header } from './components/Header/Header.js'
import { Footer } from './components/Footer/Footer.js'
import { Auth } from './components/Auth/Auth.js';
import { Registration } from './components/Registration/Registration.js'
import { Cabinet } from './components/Cabinet/Cabinet.js'


export class App extends React.Component {
  state = {
    userLogin: '',
    userPassword: '',
    isLogin: false,
    isError: false,
    errorText: '',
    loading: false
  }

  logout = () => {
    this.setState({
      userLogin: '',
      userPassword: '',
      isLogin: false,
    })
  }

  updateUserLogin = value => {
    this.setState({
      userLogin: value
    })
  }

  updateUserPassword = value => {
    this.setState({
      userPassword: value
    })
  }

  updateIsLogin = async () => {
    const login = this.state.userLogin
    const password = this.state.userPassword
    
    try {
      if (!(login && password)) {
        this.setState({
          isError: true,
          errorText : 'Заполните все поля'
        })
        return
      }

      this.setState({
        loading: true,
        isError: false
      })

      const users = await this.props.Api.getUsers()
      this.setState({ loading: false })

      for (let user in users) {
        if (login === users[user].login && password === users[user].password) {
          this.setState({ isLogin: true })
          return
        }
      }

      this.setState({ 
        isError: true,
        errorText: 'Неверный логин или пароль'
      })
    } catch (error) {
      console.log(error);
    }
  }

  resetIsError = () => {
    this.setState({
      isError: false
    })
  }


  render() {
    return (
      <React.Fragment>
        <Header
          isLogin={this.state.isLogin}
          logout={this.logout}
        />

        <main className='main'>
          <Route path='/auth' exact render={props =>
            <Auth
              updateUserLogin={this.updateUserLogin}
              updateUserPassword={this.updateUserPassword}
              updateIsLogin={this.updateIsLogin}
              resetIsError={this.resetIsError}
              isLogin={this.state.isLogin}
              isError={this.state.isError}
              errorText={this.state.errorText}
              loading={this.state.loading}
              history={props.history}
            />
          } />

          <Route path='/registration' exact render={props => 
            <Registration
              Api={this.props.Api}
              history={props.history}
            />
          } />
          <Route path='/lk' exact render={() =>
            <Cabinet
              login={this.state.userLogin}
            />
          } />
        </main>

        <Footer />
      </React.Fragment>
    )
  }
}
