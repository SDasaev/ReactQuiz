export class Api {

  static async getUsers() {
    try {
      return await (await fetch('https://react-quiz-211a2.firebaseio.com/users.json')).json()
    } catch (error) {
      console.log(error);
    }
  }

  
  static async addUser(login, password) {
    const options = {
      method: 'POST', 
      headers: {
        "Content-Type" : "application/json"
      },
      body: JSON.stringify({login, password})
    }

    try {
      await fetch('https://react-quiz-211a2.firebaseio.com/users.json', options)
    } catch (error) {
      console.log(error);
    }
  }
}